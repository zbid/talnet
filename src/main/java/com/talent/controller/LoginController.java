/**
 * 
 */
package com.talent.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p> Title: LoginController </p>
 * <p> Description: 登录验证和跳转控制器 </p>
 * <p> Company: www.honestpeak.com </p>
 * @author JinKai
 * @date 2016年7月20日  上午9:33:40
 * @version 1.0
 * 
 */
@Controller
public class LoginController {

	@RequestMapping("login.do")
	public String loginUI(){
		//不做任何操作
		return "login";//跳转到登录页面
	}
	
	public String loginProc(){
		return "";
	}
}
