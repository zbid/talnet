/**
 * 
 */
package com.talent.dao;

import java.util.List;

import com.talent.model.User;
import com.talent.page.Page;

/**
 * <p>
 * Title: UserMapper
 * </p>
 * <p>
 * Description:
 * </p>
 * <p>
 * Company: www.honestpeak.com
 * </p>
 * 
 * @author JinKai
 * @date 2016年7月19日 下午2:38:11
 * @version 1.0
 * 
 */
public interface UserDao {

	public User getUserById(Integer id);

	public User getUserByAccount(String account);

	public List<User> getUserList(Page<User> page);

	public List<User> getUserList();

	public int addUser(User user);

	public int delUser(Integer id);
}
