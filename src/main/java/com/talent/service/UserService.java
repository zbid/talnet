package com.talent.service;

import java.util.List;
import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.talent.dao.UserDao;
import com.talent.model.User;
import com.talent.page.Page;
import com.talent.util.ConfigProperties;

//@Service
public class UserService {
	private static final Logger logger = Logger.getLogger(UserService.class);
	private final int PAGE_SIZE = 10;
	@Autowired
	private UserDao userDao;

	public User getUserById(int id) {
		return userDao.getUserById(id);
	}

	public User getUserByAccount(String account) {
		return userDao.getUserByAccount(account);
	}

	public Page<User> getUserList(int pageNumber) {
		Page<User> page = new Page<User>(pageNumber, PAGE_SIZE);
		List<User> list = userDao.getUserList(page);
		page.setResultList(list);
		return page;
	}

	public List<User> getUserList() {
		List<User> list = userDao.getUserList();
		return list;
	}

	/**
	 * 用户登录成功之后 加载配置信息
	 */
	@SuppressWarnings("unused")
	public void loadConfigParameters() {
		Properties prop = ConfigProperties.getInstance().getConfigProperties();
		try {
			HttpSession session = getSession();
		} catch (Exception ex) {
			logger.debug("loadConfigParameters exception." + ex.getMessage());
			ex.printStackTrace();
		}
	}

	public int addUser(User user) {
		return userDao.addUser(user);
	}

	public int delUser(int id) {
		return userDao.delUser(id);
	}

	public static HttpSession getSession() {
		HttpSession session = null;
		try {
			session = getRequest().getSession();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return session;
	}

	public static HttpServletRequest getRequest() {
		ServletRequestAttributes attrs = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
		return attrs.getRequest();
	}
}
