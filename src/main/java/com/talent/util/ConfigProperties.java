package com.talent.util;

import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class ConfigProperties {
	private final Logger logger = Logger.getLogger(ConfigProperties.class);
	private static ConfigProperties _instance = null;
	private Properties prop = null;
	
	public ConfigProperties() {
		prop =  new Properties();
		String path = getClass().getResource("/").getPath();
		path = path.substring(1, path.indexOf("classes"));
		try  {   
			prop.load(new FileInputStream("/"+path+"config.properties"));
        }  catch  (Exception e) {
        	logger.error(e.getMessage());
            e.printStackTrace();    
        }
	}
	
	public static synchronized ConfigProperties getInstance() {
		if(_instance == null) {
			_instance = new ConfigProperties();
		}
		return _instance;
	}
	
	public Properties getConfigProperties() {
		return prop;
	}
	

}
