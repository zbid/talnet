package com.talent.util;

import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class DesTool {
	private static Cipher cipher;
	private static SecretKey securekey;
	private static KeyGenerator keyGenerator;
	private static final String CRYPT_KEY = "asd0215sdg";
	
	static {
		try {
			//Cipher：提供加密的类，"AES" 表示加密使用的算法
			cipher = Cipher.getInstance("AES");
			 //SecretKey密码生成器，用于生成一个密钥
			keyGenerator = KeyGenerator.getInstance("AES");
			SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
			secureRandom.setSeed(CRYPT_KEY.getBytes());
			keyGenerator.init(128, secureRandom); 
			securekey = keyGenerator.generateKey();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String encode(String inputData) {
		try {
			//用密钥初始化此 Cipher，Cipher.ENCRYPT_MODE 加密模式
	        cipher.init(Cipher.ENCRYPT_MODE, securekey);
	        
			byte[] result = cipher.doFinal(inputData.getBytes("utf-8"));
			
			String str = new String(Base64.encode(result, Base64.NO_PADDING+Base64.NO_WRAP+Base64.URL_SAFE));
			
			return str;
			
		} catch (Exception ex) {
			ex.printStackTrace();
		}
       return null;
	}
	
	public static String decode(String inputData) {
		try {
			//解密的时候，要用原来加密的密钥
			cipher.init(Cipher.DECRYPT_MODE, securekey);
			//解密，得到加密前的字符串
			byte[] data = Base64.decode(inputData.getBytes("utf-8"), Base64.NO_PADDING+Base64.NO_WRAP+Base64.URL_SAFE);
			
			byte[] result = cipher.doFinal(data);
			
			String str = new String(result,"utf-8");
			
			return str;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return null;
	}
	
}