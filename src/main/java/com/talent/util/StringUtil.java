package com.talent.util;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class StringUtil {

	    public final static String APP_KEY = "25wehl3uwnekw";
	    public final static String APP_SECRET = "uofPwtOUKjikR";
	    
	    private static Date date = new Date();  
	    private static StringBuilder buf = new StringBuilder();  
	    private static int seq = 0;  
	    private static final int ROTATION = 99999;  
	    public static synchronized long getTourist(){  
	      if (seq > ROTATION) seq = 0;  
	      buf.delete(0, buf.length());  
	      date.setTime(System.currentTimeMillis());  
	      String str = String.format("%1$tY%1$tm%1$td%1$tk%1$tM%1$tS%2$05d", date, seq++);  
	      return Long.parseLong(str);  
	    }  
	    
	    /**
	     * get the datetime string
	     * @return
	     */
	    public static String getStrDateTime() {
	    	Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	return format.format(new Date());
	    }
	    
	    public static String getStrDate() {
	    	Format format = new SimpleDateFormat("yyyy-MM-dd");
	    	return format.format(new Date());
	    }
	    
	    public static String getPreDayDate(int reduceDay) {
	    	Date dnow = new Date();
	    	Date dPre = new Date();
	    	Calendar calendar = Calendar.getInstance();
	    	calendar.setTime(dnow);
	    	calendar.add(Calendar.DAY_OF_MONTH, -reduceDay);
	    	dPre = calendar.getTime();
	    	
	    	Format format = new SimpleDateFormat("yyyy-MM-dd");
	    	return format.format(dPre);
	    }
	    public static boolean isEmpty(String data){
	    	if (null == data || "".equals(data.trim())||"null".equals(data.trim())) {
				return true;
			}
	    	return false;
	    }
	    
	    /**
		 * android : 所有android设备
		 * mac os : iphone ipad
		 * windows phone:Nokia等windows系统的手机
		 */
	    public static boolean isMobileDevice(String requestHeader){
			String[] deviceArray = new String[]{"android", "iphone", "windows phone","ipad", "mqqbrowser" };
			if(requestHeader == null)
				return false;
			requestHeader = requestHeader.toLowerCase();
			for(int i=0;i<deviceArray.length;i++){
				if(requestHeader.indexOf(deviceArray[i])>-1){
					return true;
				}
			}
			return false;
	    }
	  
}
