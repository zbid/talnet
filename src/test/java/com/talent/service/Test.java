/**
 * 
 */
package com.talent.service;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import com.talent.util.StringUtil;

/**
 * <p> Title: Test </p>
 * <p> Description:  </p>
 * <p> Company: www.honestpeak.com </p>
 * @author JinKai
 * @date 2016年7月19日  下午3:26:51
 * @version 1.0
 * 
 */
public class Test {

	public static void main(String[] args) {
		String strFilePath = "D:\\db_talent" + StringUtil.getStrDate() + ".sql";
		
		String strCmd = "mysqldump -u root -p123456 --databases db_talent > "+ strFilePath;
		System.out.println(execCommand(strCmd));
	}
	
	private static String execCommand(String strCmd) {
		String strRel = "success";
		try {
			Runtime runtime = Runtime.getRuntime();
			
			String[] command = new String[]{"cmd", "/c", strCmd};
			Process process = runtime.exec(command);
			
			//print out running result
			InputStreamReader in = new InputStreamReader(process.getInputStream());
			BufferedReader br = new BufferedReader(in);
		    String line = null;
		    while((line = br.readLine()) != null){
		        System.out.println(line);
		    }
		    br.close();
		    in.close();
		    strRel = (line != null) ? strRel+","+line : "success";
		    
		    //print out error messages
		    InputStreamReader in2 = new InputStreamReader(process.getErrorStream());
		 	BufferedReader br2 = new BufferedReader(in2);
		 	String line2 = null;
		 	while((line2 = br2.readLine()) != null){
		 		 System.out.println(line2);
		 	}
		 	br2.close();
		 	in2.close();
		 	strRel = (line2 != null) ? "error"+","+line2 : "error";
			
		} catch (Exception ex) {
			ex.printStackTrace();
			strRel = "error,Exception:" + ex.getMessage();
		}
		return strRel;
	}
}
