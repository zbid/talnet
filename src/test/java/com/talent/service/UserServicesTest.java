/**
 * 
 */
package com.talent.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.talent.model.User;

/**
 * <p> Title: UserServicesTest </p>
 * <p> Description:  </p>
 * <p> Company: www.honestpeak.com </p>
 * @author JinKai
 * @date 2016年7月19日  下午2:51:24
 * @version 1.0
 * 
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"classpath:spring-dao.xml"})
public class UserServicesTest {

	@Autowired
	private UserServices userServices;
	
	/**
	 * Test method for {@link com.talent.service.UserServices#getUserById(int)}.
	 */
	@Test
	public void testGetUserById() {
		User user = userServices.getUserById(1);
		System.out.println(user.getName());
	}

	/**
	 * Test method for {@link com.talent.service.UserServices#getUserByAccount(java.lang.String)}.
	 */
	@Test
	public void testGetUserByAccount() {
		User user = userServices.getUserByAccount("kaiking2");
		System.out.println(user.getName());
	}

	/**
	 * Test method for {@link com.talent.service.UserServices#getUserList()}.
	 */
	@Test
	public void testGetUserList() {
		List<User> userList = userServices.getUserList();
		for(User user : userList){
			System.out.println(user.getName());
		}
	}

	/**
	 * Test method for {@link com.talent.service.UserServices#addUser(com.talent.model.User)}.
	 */
	@Test
	public void testAddUser() {
		User user = new User();
		user.setAccount("kaiking");
		user.setName("jinkai");
		user.setPhone("13154665841");
		user.setPassword("123456");
		userServices.addUser(user);
	}

	/**
	 * Test method for {@link com.talent.service.UserServices#delUser(int)}.
	 */
	@Test
	public void testDelUser() {
		userServices.delUser(2);
	}

}
